'''
'''
from enum import Enum
import curses
import sys

class Mode(Enum):
    Normal = 1
    Insert = 2
    Visual = 3

class Instance:
    def __init__(self, bufs, offset = 6):
        self.bufs = bufs
        self.curBuf = -1
        self.onScrData = {}  #dict helps to know what is displayed on scr
        self.offset = offset
        self.buffers = []
        self.maxLine = 0

class Buffer:
    def __init__(self, lineBuf, curLine = 0):
        global Mode
        self.lineBuf = lineBuf
        self.curLine = curLine
        self.mode = Mode.Normal

class Cursor:
    def __init__(self, x, y):
        global inst
        self.x = x + inst.offset
        self.y = y

inst = None
cursor = None

def add_buffer(i):
    global inst
    b = []
    with open(sys.argv[1], "r") as f:
        for line in f.readlines():
            b.append(line)
            inst.maxLine += 1
    inst.buffers.append(Buffer(b))

def add_temp_buffer():
    global inst
    b = []
    with open("__pyviScratch__.tmp", "w") as f:
        f.write("\n")
    with open("__pyviScratch__.tmp", "r") as f:
        for line in f.readlines():
            b.append(line)
            inst.maxLine += 1
    inst.buffers.append(Buffer(b))


def render_cursor(wnd, cursor):
    wnd.move(cursor.y, cursor.x)

def render_status(wnd):
    global inst, Mode
    y,x = wnd.getmaxyx()
    if inst.buffers[inst.curBuf].mode == Mode.Normal:
        wnd.addstr(y-1,6,"Normal")
    elif inst.buffers[inst.curBuf].mode == Mode.Insert:
        wnd.addstr(y-1,6,"Insert")
    elif inst.buffers[inst.curBuf].mode == Mode.Visual:
        wnd.addstr(y-1,6,"Visual")    
            
    wnd.addstr(y-1,x-12, "%d %d"%(cursor.x ,cursor.y))

def print_text(buf, wnd):
    global inst

    wnd.clear()
    y,x = wnd.getmaxyx()
    linesAvail = y # status and cmd
    maxWidth = x - inst.offset
    i = buf.curLine
    while linesAvail > 2:
        
        if len(buf.lineBuf) <= i: # eof
            wnd.addstr(y-linesAvail, inst.offset, "~")
            linesAvail -= 1
            i += 1
        elif len(buf.lineBuf[i]) < maxWidth: #no line wrap
            wnd.addstr(y-linesAvail, 0, "%5d "%(i))
            wnd.addstr(y-linesAvail, inst.offset, buf.lineBuf[i])
            inst.onScrData[y-linesAvail] = [i,0,len(buf.lineBuf[i])] 

            linesAvail -= 1
            i += 1
        else: #linewrap
            remaining = len(buf.lineBuf[i])
            start = 0

            while remaining > 0 and linesAvail>2:
                if remaining > maxWidth:
                    end = start + maxWidth
                else:
                    end = start + remaining

                wnd.addstr(y-linesAvail, 0, "%5d"%(i))
                wnd.addstr(y-linesAvail, inst.offset, buf.lineBuf[i][start:end])
                inst.onScrData[y-linesAvail] = [i, start, end]

                remaining -= (end-start)
                start = end
                linesAvail -= 1
            i += 1

'''
    init and deinit curses
'''
def curses_wrapper(func):
    stdscr = curses.initscr()
    curses.cbreak()
    curses.noecho()
    stdscr.keypad(True) 
    
    func(stdscr)

    curses.nocbreak()
    stdscr.keypad(False)
    curses.echo()
    curses.endwin()

def move_display_down(wnd):
    global inst, inst
    y,x = wnd.getmaxyx()
    if inst.buffers[inst.curBuf].curLine < len(inst.buffers[inst.curBuf].lineBuf) - (y-3):
            inst.buffers[inst.curBuf].curLine += 1

def move_display_up(wnd):
    global inst, inst
    y,x = wnd.getmaxyx()
    if inst.buffers[inst.curBuf].curLine > 0:
            inst.buffers[inst.curBuf].curLine -= 1

def act_on_colon_command(wnd, command):
    global inst
    #print(command)
    if command =='':
        return 0
    if command == 'q!':
        return -1
    elif command == 'q':
        if inst.curBuf > 0:
            with open("__pyviBackup__%s"%(inst.bufs[inst.curBuf]), 'w') as f:
                    for s in inst.buffers[inst.curBuf].lineBuf:
                        f.write(s)
            return -1
        else:
            with open("__pyviBackup__default", 'w') as f:
                    for s in inst.buffers[inst.curBuf].lineBuf:
                        f.write(s)
            return -1
    elif command[0] == 'w':
        if len(command) == 2 and command[1] == 'q':
            with open(inst.bufs[inst.curBuf], 'w') as f:
                for s in inst.buffers[inst.curBuf].lineBuf:
                    f.write(s)
            return -1
        if len(command) == 1 and inst.curBuf >= 0:
            with open(inst.bufs[inst.curBuf], 'w') as f:
                for s in inst.buffers[inst.curBuf].lineBuf:
                    f.write(s)
        elif len(command) == 1 and inst.curBuf == -1:
            y,x = wnd.getmaxyx()
            wnd.addstr(y-1,0,"name required!")
        else:
            name = []
            for l in command[1:]:
                if l != ' ':
                    name.append(l)

            with open("".join(name), 'w') as f:
                for s in inst.buffers[inst.curBuf].lineBuf:
                    f.write(s)
    return 0

def char_log(c):
    with open("elp.txt", 'a') as f:
        f.write("%s %d"%(chr(c),c))

def process_user_input(wnd, active):
    global inst, cursor, Mode
    y,x = wnd.getmaxyx()
    #controls
    
    c = wnd.getch()
    #char_log(c)
    #wnd.addstr(y-1,14,"%d"%(c))
    #print(c)
    

    if inst.buffers[inst.curBuf].mode == Mode.Normal:
        if c == ord('j'):
            if cursor.y >= inst.maxLine:
                return active
            elif cursor.y < y-3:
                cursor.y += 1    
            elif cursor.y == y-3:
                move_display_down(wnd)
            if cursor.x - inst.offset > inst.onScrData[cursor.y][2] - inst.onScrData[cursor.y][1]:
                    cursor.x = inst.onScrData[cursor.y][2] - inst.onScrData[cursor.y][1] + inst.offset -2
        elif c == ord('k'):
            if cursor.y > 0:
                cursor.y -= 1 
            elif cursor.y == 0:
                move_display_up(wnd)
            if cursor.x - inst.offset > inst.onScrData[cursor.y][2] - inst.onScrData[cursor.y][1]:
                    cursor.x = inst.onScrData[cursor.y][2] - inst.onScrData[cursor.y][1] + inst.offset -2
        elif c == ord('h'):
            if cursor.x > inst.offset:
                cursor.x -= 1
        elif c == ord('l'):
            if cursor.x - inst.offset < inst.onScrData[cursor.y][2] - inst.onScrData[cursor.y][1]-2:
                cursor.x += 1
        elif c == ord('$'):
            if inst.onScrData[cursor.y][2] - inst.onScrData[cursor.y][1] + inst.offset < x:
                cursor.x = inst.onScrData[cursor.y][2] - inst.onScrData[cursor.y][1] + inst.offset -2
            else:
                cursor.x = x -1 
        elif c == ord('i'):
            inst.buffers[inst.curBuf].mode = Mode.Insert
        elif c == ord('a'):
            cursor.x += 1
            inst.buffers[inst.curBuf].mode = Mode.Insert
        elif c == ord('0'):
            cursor.x = inst.offset
        elif c == ord('H'):
            cursor.y = 0
        elif c == ord('M'):
            cursor.y = (y-3)//2
        elif c == ord('L'):
            cursor.y = y-3

        elif c == ord(':'):
            
            oldCursor = Cursor(cursor.x, cursor.y)
            cursor.y = y-2
            cursor.x = 0
            wnd.addch(cursor.y, cursor.x, ':')
            cursor.x += 1

            curses.echo()
            curses.nocbreak()
            command = wnd.getstr()
            command = command.decode()
            sig = act_on_colon_command(wnd, command)
            if sig == -1:
                active = not active
            curses.noecho()
            curses.cbreak()

            cursor.y = oldCursor.y
            cursor.x = oldCursor.x - inst.offset
            


        elif c == 6 : #ctrl+F
            if inst.buffers[inst.curBuf].curLine < len(inst.buffers[inst.curBuf].lineBuf) - (y-3) -y:
                inst.buffers[inst.curBuf].curLine += y
            else:
                inst.buffers[inst.curBuf].curLine = len(inst.buffers[inst.curBuf].lineBuf) - (y-3)
        elif c == 2 : #ctrl+B
            if inst.buffers[inst.curBuf].curLine >= y:
                inst.buffers[inst.curBuf].curLine -= y


    elif inst.buffers[inst.curBuf].mode == Mode.Insert:
        if c == 27: #esc
            inst.buffers[inst.curBuf].mode = Mode.Normal
        elif c == 8 or c == 263: #backspace
            newline = []
            if inst.onScrData[cursor.y][0] != 0 or cursor.x - inst.offset > 0:
                if cursor.x - inst.offset > 0:
                    for k,v in enumerate(inst.buffers[inst.curBuf].lineBuf[inst.onScrData[cursor.y][0]]):
                        if k != inst.onScrData[cursor.y][1] + cursor.x - inst.offset-1:
                            newline.append(v)
                    cursor.x -= 1

                elif inst.onScrData[cursor.y][0]-1 >= 0:
                    for v in inst.buffers[inst.curBuf].lineBuf[inst.onScrData[cursor.y-1][0]]:
                        if not v in "\n":
                            newline.append(v)
                    for v in inst.buffers[inst.curBuf].lineBuf[inst.onScrData[cursor.y][0]]:
                        newline.append(v)

                    cursor.x = len(inst.buffers[inst.curBuf].lineBuf[inst.onScrData[cursor.y-1][0]]) + inst.offset
                    cursor.y -= 1
                    
                    del inst.buffers[inst.curBuf].lineBuf[inst.onScrData[cursor.y][0]]
                inst.buffers[inst.curBuf].lineBuf[inst.onScrData[cursor.y][0]] = "".join(newline)
            


        elif c == 330: #delete might not work if ascii is different from orig keyboard
            if inst.onScrData[cursor.y][0] != len(inst.buffers[inst.curBuf].lineBuf) -1 or cursor.x - inst.offset < inst.onScrData[cursor.y][2] - inst.onScrData[cursor.y][1]:
                newline = []
                if cursor.x - inst.offset < len(inst.buffers[inst.curBuf].lineBuf[inst.onScrData[cursor.y][0]])-1:
                    for k,v in enumerate(inst.buffers[inst.curBuf].lineBuf[inst.onScrData[cursor.y][0]]):
                        if k != inst.onScrData[cursor.y][1] + cursor.x - inst.offset:
                                newline.append(v)
                else:
                    for v in inst.buffers[inst.curBuf].lineBuf[inst.onScrData[cursor.y][0]]:
                        if not v in "\n":
                            newline.append(v)
                    for v in inst.buffers[inst.curBuf].lineBuf[inst.onScrData[cursor.y][0]+1]:
                            newline.append(v)
                    del inst.buffers[inst.curBuf].lineBuf[inst.onScrData[cursor.y][0]+1]

                inst.buffers[inst.curBuf].lineBuf[inst.onScrData[cursor.y][0]] = "".join(newline)



        elif c == 10: #enter might not work if ascii is different from orig keyboard
            newline = []
            newline1 = []
            broken = False
            for k,v in enumerate(inst.buffers[inst.curBuf].lineBuf[inst.onScrData[cursor.y][0]]):
                if k == inst.onScrData[cursor.y][1] + cursor.x - inst.offset:
                    newline1.append(v)
                    broken = True
                    newline.append("\n")
                elif not broken:
                    newline.append(v)
                else:
                    newline1.append(v)

            del inst.buffers[inst.curBuf].lineBuf[inst.onScrData[cursor.y][0]]
            inst.buffers[inst.curBuf].lineBuf.insert(inst.onScrData[cursor.y][0] ,"".join(newline))
            if cursor.y+1 in inst.onScrData.keys():
                inst.buffers[inst.curBuf].lineBuf.insert(inst.onScrData[cursor.y+1][0] ,"".join(newline1))
            else:
                inst.buffers[inst.curBuf].lineBuf.append("".join(newline1))
            cursor.y += 1
            cursor.x = inst.offset
        elif c == 9: #tab
            newline = []
            for k,v in enumerate(inst.buffers[inst.curBuf].lineBuf[inst.onScrData[cursor.y][0]]):
                if k == inst.onScrData[cursor.y][1] + cursor.x - inst.offset:
                    newline.append(' '*4)
                newline.append(v)


            inst.buffers[inst.curBuf].lineBuf[inst.onScrData[cursor.y][0]] = "".join(newline)
            cursor.x += 4 #tabsize

        else: #something typeable
            
            newline = []
            for k,v in enumerate(inst.buffers[inst.curBuf].lineBuf[inst.onScrData[cursor.y][0]]):
                if k == inst.onScrData[cursor.y][1] + cursor.x - inst.offset:
                    newline.append(chr(c))
                newline.append(v)


            inst.buffers[inst.curBuf].lineBuf[inst.onScrData[cursor.y][0]] = "".join(newline)
            cursor.x += 1

    return active

'''
    runs the program logic
'''
def runner(wnd):
    global inst, cursor

    

    if len(inst.buffers) == 0:
        inst.buffers.append(Buffer([" "]))
    active = True
    while active:
        print_text(inst.buffers[0],wnd)
        render_status(wnd)
        render_cursor(wnd, cursor)
        active = process_user_input(wnd, active)
        

def main():
    global inst, cursor

    inst = Instance(sys.argv[1:])
    cursor = Cursor(0,0)
    if len(sys.argv) > 1:
        add_buffer(1)
        inst.curBuf = 0
    else:
        add_temp_buffer()
        inst.curBuf = 0
    curses_wrapper(runner)

if __name__ == '__main__':
    main() 